using Payroll.Data.DataProvider;
using Payroll.Data.Interfaces;
using Payroll.Service.Interfaces;
using Payroll.Service.Services;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace Payroll.App
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            
            container.RegisterType<IEmployeesService, EmployeesService>();
            container.RegisterType<IEmployeesProvider, EmployeesProvider>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}