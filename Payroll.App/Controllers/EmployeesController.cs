﻿using System.Web.Http;
using System.Threading.Tasks;
using Payroll.Service.Interfaces;

namespace Payroll.App.Controllers
{
    [RoutePrefix("api/employees")]
    public class EmployeesController : ApiController
    {

        private readonly IEmployeesService _employeesService;

        public EmployeesController(IEmployeesService employeesService)
        {
            _employeesService = employeesService;
        }

        // GET: ~/getemployees
        [HttpGet]
        [Route("getemployees")]
        public async Task<IHttpActionResult> Get()
        {
            var employees = await _employeesService.Get();

            return Ok(employees);
        }

        // GET: ~/getemployee/1
        [HttpGet]
        [Route("getemployee")]
        public async Task<IHttpActionResult> GetEmployee(int id)
        {
            var employee = await _employeesService.GetEmployee(id);

            return Ok(employee);
        }
    }
}
