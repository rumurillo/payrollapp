﻿using Payroll.Models.Models;
using Payroll.Service.EmployeesFactory.Helpers;

namespace Payroll.Test.Helpers
{
    public class EmployeeData
    {
        public static Employee GetHourlyEmployee = new Employee
        {
            Id = 1,
            Name = "Ruben",
            ContractTypeName = nameof(EmployeeContractType.HourlySalaryEmployee),
            HourlySalary = 10000,
            MonthlySalary = 19000
        };

        public static Employee GetMonthlyEmployee = new Employee
        {
            Id = 2,
            Name = "Francy",
            ContractTypeName = nameof(EmployeeContractType.MonthlySalaryEmployee),
            HourlySalary = 152000,
            MonthlySalary = 415000
        };
    }
}
