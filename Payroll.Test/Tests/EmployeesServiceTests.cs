﻿using Payroll.Models.DTO;
using Payroll.Test.Configs;
using Payroll.Test.Helpers;
using Payroll.Service.EmployeesFactory.Factory;
using Payroll.Service.EmployeesFactory.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Payroll.Test
{
    [TestClass]
    public class EmployeesServiceTests
    {
        private static ConcreteEmployeeFactory ConcreteEmployeeFactory;

        public EmployeesServiceTests()
        {
            // Register Mapper mappings.
            ConfigTests.RegisterMaps();
        }

        [TestMethod]
        public void Factory_Method_Returns_HourlySalaryEmployeeDto()
        {
            // Arrange
            ConcreteEmployeeFactory = new ConcreteEmployeeFactory();

            // Act
            var employee = ConcreteEmployeeFactory.GetEmployeeDto(EmployeeContractType.HourlySalaryEmployee, EmployeeData.GetHourlyEmployee);

            // Assert
            Assert.IsInstanceOfType(employee, typeof(HourlySalaryEmployeeDto));

        }

        [TestMethod]
        public void Factory_Method_Returns_MonthlySalaryEmployeeDto()
        {
            // Arrange
            ConcreteEmployeeFactory = new ConcreteEmployeeFactory();

            // Act
            var employee = ConcreteEmployeeFactory.GetEmployeeDto(EmployeeContractType.MonthlySalaryEmployee, EmployeeData.GetMonthlyEmployee);

            // Assert
            Assert.IsInstanceOfType(employee, typeof(MonthlySalaryEmployeeDto));

            // 
        }

        [TestCleanup]
        public void Test_Cleanup()
        {
            ConfigTests.ResetMaps();
        }
}
}
