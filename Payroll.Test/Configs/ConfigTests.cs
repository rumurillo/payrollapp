﻿using Payroll.Service.AutoMapper;

namespace Payroll.Test.Configs
{
    public static class ConfigTests
    {
        public static void RegisterMaps()
        {
            AutoMapperConfigs.RegisterMaps();
        }

        public static void ResetMaps()
        {
            AutoMapperConfigs.Reset();
        }
    }
}
