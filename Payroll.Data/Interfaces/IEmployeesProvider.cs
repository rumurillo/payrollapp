﻿using Payroll.Models.Models;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Payroll.Data.Interfaces
{
    public interface IEmployeesProvider
    {
        Task<IEnumerable<Employee>> Get();

        Task<Employee> GetEmployee(int id);
    }
}
