﻿using System;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using Payroll.Models.Models;
using System.Threading.Tasks;
using Payroll.Data.Interfaces;
using System.Net.Http.Headers;
using System.Collections.Generic;

namespace Payroll.Data.DataProvider
{
    /// <summary>
    /// Class that serves as data provider by accessing the data in the MAS Global Employee API.
    /// </summary>
    public class EmployeesProvider : IEmployeesProvider
    {
        private static IEnumerable<Employee> Employees;
        private static readonly HttpClient HttpClient;
        private const string BaseAddress = "http://masglobaltestapi.azurewebsites.net/api/";
        private const string EmployeesUrl = "employees";

        static EmployeesProvider()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(BaseAddress),
            };
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Gets all employees from API.
        /// </summary>
        /// <returns>List of employees.</returns>
        private async Task<IEnumerable<Employee>> GetEmployeesAsync()
        {
            var result = await HttpClient.GetStringAsync(EmployeesUrl);

            Employees = JsonConvert.DeserializeObject<IEnumerable<Employee>>(result);

            return Employees;
        }

        /// <summary>
        /// Returns all the employees obtained from GetEmployeesAsync, i.e. result of API.
        /// </summary>
        /// <returns>List of all employees.</returns>
        public async Task<IEnumerable<Employee>> Get()
        {
            if (Employees == null)
                await GetEmployeesAsync();

            return Employees;
        }

        /// <summary>
        /// Gets a specific employee filtered by id.
        /// </summary>
        /// <param name="id">User's unique identifier</param>
        /// <returns>Employee</returns>
        public async Task<Employee> GetEmployee(int id)
        {
            if (Employees == null)
                await GetEmployeesAsync();

            var employee = Employees
                .Where(e => e.Id == id)
                .FirstOrDefault();

            return employee;
        }
    }
}
