﻿using System.Threading.Tasks;
using Payroll.Models.Interfaces;
using System.Collections.Generic;

namespace Payroll.Service.Interfaces
{
    public interface IEmployeesService
    {
        Task<IEnumerable<IEmployee>> Get();

        Task<IEmployee> GetEmployee(int id);
    }
}
