﻿using System.Threading.Tasks;
using Payroll.Data.Interfaces;
using Payroll.Models.Interfaces;
using System.Collections.Generic;
using Payroll.Service.Interfaces;
using Payroll.Service.EmployeesFactory.Factory;
using Payroll.Service.EmployeesFactory.Helpers;

namespace Payroll.Service.Services
{
    public class EmployeesService : IEmployeesService
    {
        private readonly IEmployeesProvider EmployeesProvider;
        public EmployeesService(IEmployeesProvider employeesProvider)
        {
            EmployeesProvider = employeesProvider;
        }

        /// <summary>
        /// Get all employees using Employee Provider.
        /// This Get method uses the already-existing GetEmployee(int id) method that uses the Factory Method to create
        /// the employee DTO at run-time based on the Employee's contract type.
        /// </summary>
        /// <returns>List of employees.</returns>
        public async Task<IEnumerable<IEmployee>> Get()
        {
            var employees = await EmployeesProvider.Get();

            List<IEmployee> employeesDtos = new List<IEmployee>();
            foreach (var employee in employees)
            {
                var employeeDto = await GetEmployee(employee.Id);
                employeesDtos.Add(employeeDto);
            }

            return employeesDtos;
        }

        /// <summary>
        /// Get an employee filtered by id.
        /// Once the employee is obtained, the DTO is created at run time based on the employee's contract type.
        /// </summary>
        /// <param name="id">User's unique identifier.</param>
        /// <returns></returns>
        public async Task<IEmployee> GetEmployee(int id)
        {
            var employee = await EmployeesProvider.GetEmployee(id);

            EmployeeFactory factory = new ConcreteEmployeeFactory();

            var employeeContractType = employee.ContractTypeName == nameof(EmployeeContractType.HourlySalaryEmployee)
                ? EmployeeContractType.HourlySalaryEmployee
                : EmployeeContractType.MonthlySalaryEmployee;

            var employeeDto = factory.GetEmployeeDto(employeeContractType, employee);

            return employeeDto;
        }
    }
}
