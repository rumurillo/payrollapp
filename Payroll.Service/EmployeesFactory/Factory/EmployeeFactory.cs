﻿
using Payroll.Models.Models;
using Payroll.Models.Interfaces;
using Payroll.Service.EmployeesFactory.Helpers;

namespace Payroll.Service.EmployeesFactory.Factory
{
    /// <summary>
    /// Employee Factory - abstracts the creation of the Employee Dto.
    /// </summary>
    public abstract class EmployeeFactory
    {
        public abstract IEmployee GetEmployeeDto(EmployeeContractType contractType, Employee employee);
    }
}
