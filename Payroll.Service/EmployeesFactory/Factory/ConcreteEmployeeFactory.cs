﻿using System;
using AutoMapper;
using Payroll.Models.DTO;
using Payroll.Models.Interfaces;
using Payroll.Models.Models;
using Payroll.Service.EmployeesFactory.Helpers;

namespace Payroll.Service.EmployeesFactory.Factory
{
    /// <summary>
    /// Concreate Creacional class - Creates an instance of an employee Dto based on the contract type.
    /// Additionally, it calculates the salary based on the employee's contract type.
    /// </summary>
    public class ConcreteEmployeeFactory : EmployeeFactory
    {
        public override IEmployee GetEmployeeDto(EmployeeContractType contractType, Employee employee)
        {
            switch (contractType)
            {
                case EmployeeContractType.HourlySalaryEmployee:
                    var hourlySalaryEmployeeDto = Mapper.Map<Employee, HourlySalaryEmployeeDto>(employee);
                    hourlySalaryEmployeeDto.Salary = 120 * employee.HourlySalary * 12;
                    return hourlySalaryEmployeeDto;

                case EmployeeContractType.MonthlySalaryEmployee:
                    var monthlySalaryEmployeeDto = Mapper.Map<Employee, MonthlySalaryEmployeeDto>(employee);
                    monthlySalaryEmployeeDto.Salary = employee.MonthlySalary * 12;
                    return monthlySalaryEmployeeDto;

                default:
                    throw new Exception($"Unable to retrieve data for employee with contract type: {contractType}");
            }
        }
    }
}
