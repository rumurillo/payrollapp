﻿namespace Payroll.Service.EmployeesFactory.Helpers
{
    public enum EmployeeContractType
    {
        HourlySalaryEmployee,
        MonthlySalaryEmployee
    }
}
