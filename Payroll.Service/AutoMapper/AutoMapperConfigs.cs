﻿using AutoMapper;
using Payroll.Models.DTO;
using Payroll.Models.Models;

namespace Payroll.Service.AutoMapper
{
    public class AutoMapperConfigs
    {
        /// <summary>
        /// Registers mapping of objects.
        /// </summary>
        public static void RegisterMaps()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Employee, HourlySalaryEmployeeDto>();
                config.CreateMap<Employee, MonthlySalaryEmployeeDto>();
            });
            Mapper.AssertConfigurationIsValid();
        }

        /// <summary>
        /// Resets mappings.
        /// </summary>
        public static void Reset()
        {
            Mapper.Reset();
        }
    }
}